# recupero degli impiegati che hanno superato tutti i corsi di un impiegato xx, con annesso codice corso e voto relativo;
# NB: Prendo solo gli impiegati che hanno superato il corso tramite esame, dato che sono interessato al voto per l'analisi

SELECT i.*, le.codice_corso, le.voto
FROM impiegato i
JOIN libretto le ON i.codice = cast(le.impiegato_codice as integer)
WHERE le.voto is not null 
	and not exists (
		SELECT *
		FROM libretto l
		WHERE cast(l.impiegato_codice as integer) = 4647
			AND not exists
			(
				SELECT *
				FROM libretto l1
				WHERE cast(l1.impiegato_codice as integer) = i.codice
				AND l1.corso_codice = l.corso_codice
			)
	) 

# recupero degli impiegati che hanno superato tutti i corsi di un impiegato xx; (COM'ERA PRIMA)

SELECT *
FROM impiegato i
WHERE not exists (
		SELECT *
		FROM libretto l
		WHERE cast(l.impiegato_codice as integer) = 4647
			AND not exists
			(
				SELECT *
				FROM libretto l1
				WHERE cast(l1.impiegato_codice as integer) = i.codice
				AND l1.corso_codice = l.corso_codice
			)
	)

-------------       CODICE R (BASATO SULLA PRIMA DELLE DUE QUERY SOPRA)      -------------
res <- dbGetQuery(con, "SELECT i.codice, i.area, le.voto
	FROM impiegato i
	JOIN libretto le ON i.codice = cast(le.impiegato_codice as integer)
	WHERE le.voto is not null 
		and not exists (
			SELECT *
			FROM libretto l
			WHERE cast(l.impiegato_codice as integer) = 4647
				AND not exists
				(
					SELECT *
					FROM libretto l1
					WHERE cast(l1.impiegato_codice as integer) = i.codice
					AND l1.corso_codice = l.corso_codice
				)
		)")
boxplot(res$voto ~ res$area, tpe="p", xlab="Aree aziendali", ylab="Voto esami superati", main="Distribuzione delle votazioni degli esami superati dai dipendenti che hanno superato tutti gli esami di 4647")
abline(h=(seq(0,100, 5)), col="lightgray", lty="solid")
boxplot(res$voto ~ res$area, tpe="p", xlab="Aree aziendali", ylab="Voto esami superati", main="Distribuzione delle votazioni degli esami superati dai dipendenti che hanno superato tutti gli esami di 4647", add=T)
stripchart(res$voto ~ res$area, method="jitter", pch=19, col="#5d81d4", vertical=TRUE, add=TRUE)
-----------------------------------------------



# Recupera gli utenti che hanno superato alcuni, ma non tutti, i corsi dell’ impiegato x (sottoinsieme proprio)

SELECT area, COUNT(area) AS n_dip
FROM impiegato i
WHERE exists (
		SELECT *
		FROM libretto l
		WHERE cast(l.impiegato_codice as integer) = '6151'
			AND exists (
				SELECT *
				FROM libretto l1
				WHERE cast(l1.impiegato_codice as integer) = i.codice
					AND l1.corso_codice = l.corso_codice
			)
	) and exists (
		SELECT *
		FROM libretto l
		WHERE cast(l.impiegato_codice as integer) = '6151'
			AND not exists (
				SELECT *
				FROM libretto l1
				WHERE cast(l1.impiegato_codice as integer) = i.codice
					AND l1.corso_codice = l.corso_codice
			)
	)
GROUP BY area

-------------       CODICE R       -------------
v_nomi<-readLines("C:/Users/simon/Desktop/nomi.txt")
v_cognomi<-readLines("C:/Users/simon/Desktop/cognomi.txt")
v_dipartimenti <- c("Vendita", "RD", "Finanze", "Controllo qualità", "IT", "Risorse umane", "Produzione", "Marketing", "URP", "Logistica", "Amministrazione")
impiegati_df <- data.frame(codice = sample(1:10000, 1500, replace=F), nome = sample (v_nomi, 1500, replace=T), cognome = sample(v_cognomi, 1500, replace=T), area=sample(v_dipartimenti, 1500, replace=T))
dbWriteTable(con, name="impiegato", value=impiegati_df, append=T)

cod_df <- dbGetQuery(con, "select codice from impiegato")
v_codiceImpiegati <- cod_df$codice
codCorso_df <- dbGetQuery(con, "select codice from corso")
v_codiceCorso <- codCorso_df$codice
libretto_df <- data.frame(impiegato_codice=sample(v_codiceImpiegati,2000, replace=T), corso_codice=sample(v_codiceCorso,2000,replace=T), esame_id=sample(1:10000, 2000, replace=F), voto=sample(60:100, 2000, replace=T))
dbWriteTable(con, name="libretto", value=libretto_df, append=T)

res <- dbGetQuery(con, "SELECT area, COUNT(area) AS n_dip
	FROM impiegato i
	WHERE exists (
			SELECT *
			FROM libretto l
			WHERE cast(l.impiegato_codice as integer) = '6151'
				AND exists (
					SELECT *
					FROM libretto l1
					WHERE cast(l1.impiegato_codice as integer) = i.codice
						AND l1.corso_codice = l.corso_codice
				)
		) and exists (
			SELECT *
			FROM libretto l
			WHERE cast(l.impiegato_codice as integer) = '6151'
				AND not exists (
					SELECT *
					FROM libretto l1
					WHERE cast(l1.impiegato_codice as integer) = i.codice
						AND l1.corso_codice = l.corso_codice
				)
		)
	GROUP BY area")
data <- data.frame(name=res$area, value=as.integer(res$n_dip))

plotSubset <- barplot(data$value ,names.arg=data$name, xlab="Aree aziendali", ylab="Impiegati", col=c("#919191", "#aaaaaa", "#c8c8c8", "#9b9b9b", "#b9b9b9", "#878787", "#a5a5a5","#7d7d7d", "#737373", "#aaaaaa", "#a5a5a5" ),main="Impiegati che hanno superato un sottoinsieme di esami dell'impiegato con codice 6151 suddivisi per area di appartenenza", border="black", yaxp=c(0, max(data$value), 5))
abline(h=(seq(0,50,2)), col="lightgray", lty="solid")
axis(side=2, at=seq(0, 50, by=5))
plotSubset <- barplot(data$value ,names.arg=data$name, xlab="Aree aziendali", ylab="Impiegati", col=c("#919191", "#aaaaaa", "#c8c8c8", "#9b9b9b", "#b9b9b9", "#878787", "#a5a5a5","#7d7d7d", "#737373", "#aaaaaa", "#a5a5a5" ),main="Impiegati che hanno superato un sottoinsieme di esami dell'impiegato con codice 6151 suddivisi per area di appartenenza", border="black", yaxp=c(0, max(data$value), 5), add=TRUE)
-----------------------------------------------

# recupero degli eventuali esami di una certa edizione e i risultatati degli studenti, con annesse le loro informazioni

SELECT e.*, i.*
FROM esame ex
JOIN impiegato i ON i.codice = ex.codice_studente
JOIN edizione e ON e.id = ex.edizione_id
WHERE e.corso_codice = 'CXYZ'
	AND e.codice = 12345
	AND e.anno = 2999


# recupero numero dei docenti che possono insegnare in un certo corso (un dipendente può insegnare solo se ha superato il corso) divisi per area

SELECT i.area, COUNT(*)
FROM impiegato i
WHERE exists
	(
		SELECT *
		FROM libretto l
		WHERE l.impiegato_codice = i.codice
			AND l.corso_codice = '05'
	)
GROUP BY i.area

-------------       CODICE R       -------------
count_corsoA <- dbGetQuery(con,"SELECT i.area, COUNT(*)
	FROM impiegato i
	WHERE exists
		(
			SELECT *
			FROM libretto l
			WHERE l.impiegato_codice = i.codice
				AND l.corso_codice = '01'
		)
	GROUP BY i.area")
matrice <- cbind(as.integer(count_corsoA$count), as.integer(count_corsoB$count), as.integer(count_corsoC$count), as.integer(count_corsoD$count), as.integer(count_corsoE$count), as.integer(count_corsoF$count), as.integer(count_corsoG$count), as.integer(count_corsoH$count), as.integer(count_corsoI$count), as.integer(count_corsoJ$count), as.integer(count_corsoK$count), as.integer(count_corsoL$count), as.integer(count_corsoM$count), as.integer(count_corsoN$count), as.integer(count_corsoO$count), as.integer(count_corsoP$count))
colnames(matrice) <- v_codiceCorso
temp <- dbGetQuery(con, "SELECT i.area, COUNT(*)
	FROM impiegato i
	WHERE exists
		(
			SELECT *
			FROM libretto l
			WHERE l.impiegato_codice = i.codice
				AND l.corso_codice = '05'
		)
	Group BY i.area")
rownames(matrice) <- temp$area

barplot(matrice, beside = TRUE, col=c("#1b2631", "#34495e", "#596169", "#21618c", "#2e86c1", "#85c1e9", "#48c9b0", "#d2d4d4", "#76d7c4", "#8b9394", "#148f77"), xlab="Corsi", ylab="Dipendenti", main="Totale di impiegati per ogni area che hanno superato l'esame del relativo corso", legend.text =rownames(matrice), args.legend = list(x = "topright", inset = c(-0.05, -0.02), ncol=3, text.width=14, bty="n"), ylim=c(0,25))
abline(h=(seq(0,25,2)), col="lightgray", lty="solid")
barplot(matrice, beside = TRUE, col=c("#1b2631", "#34495e", "#596169", "#21618c", "#2e86c1", "#85c1e9", "#48c9b0", "#d2d4d4", "#76d7c4", "#8b9394", "#148f77"), xlab="Corsi", ylab="Dipendenti", main="Totale di impiegati per ogni area che hanno superato l'esame del relativo corso", legend.text =rownames(matrice), args.legend = list(x = "topright", inset = c(-0.05, -0.02), ncol=3, text.width=14, bty="n"), ylim=c(0,25), add=T)
-----------------------------------------------