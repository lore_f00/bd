CREATE TABLE impiegato (
	codice serial primary key,
	nome varchar(255) not null,
	cognome varchar(255) not null,
	indirizzo varchar(255) not null,
	telefono varchar(15) not null,
	area varchar(255) not null,
	mansione varchar(255) not null,
	CONSTRAINT user_phone_unique UNIQUE(telefono)
);

CREATE TABLE corso (
	codice varchar(10) primary key,
	titolo varchar(255) not null,
	descrizione text not null
);

CREATE TABLE prerequisiti (
	corso_codice varchar(10) not null,
	prerequisito_codice varchar(10) not null,
	CONSTRAINT pk_prerequisisti PRIMARY KEY(corso_codice, prerequisito_codice),
	CONSTRAINT fk_corso FOREIGN KEY(corso_codice) REFERENCES corso(codice) ON delete cascade ON update cascade,
	CONSTRAINT fk_prerequisito FOREIGN KEY(prerequisito_codice) REFERENCES corso(codice) ON delete cascade ON update cascade
);

CREATE TABLE edizione (
	id serial primary key,
	corso_codice varchar(10) not null,
	codice int not null,
	anno int not null,
	data_inizio date not null,
	data_fine date not null,
	giorni_settimana varchar(255) not null,
	numero_studenti_coinvolti int not null,
	CONSTRAINT check_end_after_start CHECK (data_inizio < data_fine),
	CONSTRAINT check_invited_students CHECK (numero_studenti_coinvolti >= 0),
	CONSTRAINT fk_corso FOREIGN KEY(corso_codice) REFERENCES corso(codice) ON delete cascade ON update cascade,
	CONSTRAINT corso_anno_unique UNIQUE(corso_codice, codice, anno)
);

CREATE TABLE esame (
	id serial primary key,
	edizione_id int not null,
	codice_studente int not null,
	data_svolgimento timestamp not null,
	voto int not null,
	CONSTRAINT check_voto CHECK (voto BETWEEN 1 AND 10),
	CONSTRAINT fk_edizione FOREIGN KEY(edizione_id) REFERENCES edizione(id) ON delete cascade ON update cascade,
	CONSTRAINT fk_studente FOREIGN KEY(codice_studente) REFERENCES impiegato(codice) ON delete cascade ON update cascade
);

CREATE TYPE tipi_lezione AS ENUM ('presenza', 'remoto');

CREATE TABLE lezione (
	codice int primary key,
	edizione_id int not null,
	tipo tipi_lezione not null,
	aula varchar(20),
	link text,
	data date not null,
	ora_inizio time not null,
	ora_fine time not null,
	CONSTRAINT lezione_edizione UNIQUE(codice, edizione_id),
	CONSTRAINT fk_edizione FOREIGN KEY(edizione_id) REFERENCES edizione(id) ON delete cascade ON update cascade,
	CONSTRAINT check_lezione_remoto CHECK (tipo = 'remoto' AND link is not null AND aula is null),
	CONSTRAINT check_lezione_presenza CHECK (tipo = 'presenza' AND link is null AND aula is not null),
	CONSTRAINT check_end_after_start CHECK (ora_inizio < ora_fine)
);

CREATE INDEX tipo_lezione_idx ON lezione(tipo); # indice con i btree https://www.postgresql.org/docs/9.1/sql-createindex.html

CREATE TYPE ruoli_prendere_parte AS ENUM ('studente', 'insegnante');

CREATE TABLE prendere_parte (
  edizione_id int not null,
  impiegato_codice int not null,
  ruolo ruoli_prendere_parte not null,
  CONSTRAINT pk_prendere_parte PRIMARY KEY (edizione_id, impiegato_codice),
  CONSTRAINT fk_edizione FOREIGN KEY(edizione_id) REFERENCES edizione(id) ON delete cascade ON update cascade,
  CONSTRAINT fk_impiegato FOREIGN KEY(impiegato_codice) REFERENCES impiegato(codice) ON delete cascade ON update cascade
);

CREATE TABLE tenuta_da (
  lezione_codice int not null,
  insegnante_codice int not null,
  CONSTRAINT pk_tenuta_da PRIMARY KEY (lezione_codice, insegnante_codice),
  CONSTRAINT fk_lezione FOREIGN KEY(lezione_codice) REFERENCES lezione(codice) ON delete cascade ON update cascade,
  CONSTRAINT fk_insegnante FOREIGN KEY(insegnante_codice) REFERENCES impiegato(codice) ON delete cascade ON update cascade
);

CREATE TABLE libretto (
  impiegato_codice int not null,
  corso_codice varchar not null,
  esame_id int,
  voto int,
  CONSTRAINT pk_libretto PRIMARY KEY (impiegato_codice, corso_codice),
  CONSTRAINT fk_corso FOREIGN KEY(corso_codice) REFERENCES corso(codice) ON delete cascade ON update cascade,
  CONSTRAINT fk_impiegato FOREIGN KEY(impiegato_codice) REFERENCES impiegato(codice) ON delete cascade ON update cascade,
  CONSTRAINT fk_esame FOREIGN KEY(esame_id) REFERENCES esame(id) ON delete cascade ON update cascade,
  CONSTRAINT check_esame_voto_not_null CHECK ((esame_id is null AND voto is null) OR (esame_id is not null AND voto is not null))
);

create or replace function check_prendere_parte_func_studente()
returns trigger
language plpgsql as
$$
begin
	perform *
	from prerequisiti p
	join edizione e on e.corso_codice = p.corso_codice # join per recuperare il corso dell'edizione
	where e.id = new.edizione_id
		and not exists ( # mi assicuro che l'utente abbia superato ogni prerequisito del corso
			select *
			from libretto l
			where l.impiegato_codice = i.codice
				and l.corso_codice = e.corso_codice
		)

	if found
		then raise exception 'Lo studente non ha superato uno o più requisiti';
	end if;

	return new;
end;
$$;

create or replace function check_prendere_parte_func_insegnante()
returns trigger
language plpgsql as
$$
begin
	perform *
	from libretto l
	join edizione e on e.corso_codice = l.corso_codice
	where l.impiegato_codice = new.impiegato_codice # prendo il libretto dello studente
		and l.corso_codice = e.corso_codice # prendo il record in cui il corso è quello della edizione a cui sto aggiungendo l'utente
		and e.id = new.edizione_id;

	if not found
		then raise exception 'L insegnante non ha superato il corso';
	end if;

	return new;
end;
$$;

CREATE OR REPLACE TRIGGER trigger_prendere_parte_insegnante
before update or insert on "prendere_parte"
for each row
when (new.ruolo = 'insegnante')
execute procedure check_prendere_parte_func_insegnante();

CREATE OR REPLACE TRIGGER trigger_prendere_parte_studente
before update or insert on "prendere_parte"
for each row
when (new.ruolo = 'studente')
execute procedure check_prendere_parte_func_studente();